var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
var createError = require('http-errors');
var authentication = {


    createToken(userID) {
        let token = jwt.sign({
            id: userID
        }, config.secret, {
            expiresIn: 86400
        }, );
        return token;
    },

    verifyToken(token, next) {
        if (!token) {
            return next(createError(401))
        } else {
            jwt.verify(token, config.secret, function (err, decoded) {     
                if (err) {
                    return next(createError(401))
                }
            })
        }


    }
}

module.exports = authentication;