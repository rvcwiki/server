var db = require('../dbconnection');
var search = {
    searchName: function (dataRequest, callback) {
        console.log(dataRequest)
        let sql;

        if (dataRequest.key == "Name") {
            sql = "WHERE lower(project.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
        } else if (dataRequest.key == "Type") {
            sql = "WHERE lower(issuetype.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
        } else if (dataRequest.key == "Status") {
            sql = "WHERE lower(issuestatus.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
        } else if (dataRequest.key == "All") {
            sql = "WHERE lower(PROJECT) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
            sql += "OR lower(project.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
            sql += "OR lower(SUMMARY) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
            sql += "OR lower(issuetype.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
            sql += "OR lower(issuestatus.pname) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
        }


        var searchSQL;
        searchSQL = "SELECT PROJECT as pjID, project.pname as pjName " ;
        searchSQL += "FROM (((jiraissue INNER JOIN project ON jiraissue.PROJECT = project.ID)";
        searchSQL += "INNER JOIN issuetype ON jiraissue.issuetype = issuetype.ID) ";
        searchSQL += "INNER JOIN issuestatus ON jiraissue.issuestatus = issuestatus.ID) " + sql + "GROUP BY project.pname";

        return db.query(searchSQL, callback);
    }

};
module.exports = search;