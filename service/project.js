var db = require('../dbconnection');
var project = {
    getAllProject: function (dataRequest, callback) {
        console.log(dataRequest)

        try {
            var projectSQL;

            projectSQL = "SELECT pjID,pjName,stDone,stInProgress,stOpen,stDone+stInProgress+stOpen as Total FROM ";
            projectSQL += "(SELECT PJ.ID as pjID ,PJ.pname AS pjName ,IFNULL(sum(JI.issuestatus=10001),0) as stDone, IFNULL(sum(JI.issuestatus=3),0) as stInprogress,IFNULL(sum(JI.issuestatus=10000),0) as stOpen ";
            projectSQL += "FROM project PJ LEFT JOIN jiraissue  JI ON PJ.ID = JI.PROJECT  AND JI.issuetype IN (10000,10002,10003) ";
            projectSQL += "GROUP BY PJ.ID) T1 ";

            if (dataRequest.key == "Name") {
                projectSQL += "WHERE lower(T1.pjName) LIKE '%" + dataRequest.value.toLowerCase().trim() + "%' ";
            }

            return db.query(projectSQL, callback);
        } catch (error) {

            console.log("error" + JSON.stringify(error));
        }
    },



};
module.exports = project;