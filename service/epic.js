var db = require('../dbconnection');
var epic = {
    getEpic: function (id, callback) {
        var epicSQL;

        epicSQL = "SELECT Epic AS SSS,Epic_Des ,count(*) as Subtask, sum(case when t.status='Done' then 1 else 0 end) as Done , ROUND (sum(case when t.status='Done' then 1 else 0 end)/count(*)  *100 ,2) as Progress FROM ";
        epicSQL += "(SELECT e1.PROJECT AS ID, e1.ID AS SSS, CFV.STRINGVALUE AS Epic ,e1.SUMMARY AS Epic_Des, s1.SUMMARY AS Subtask_des, (SELECT pname FROM issuestatus JS WHERE JS.ID=s1.ISSUESTATUS) STATUS FROM ";
        epicSQL += "(SELECT JIE.*, ILE.SOURCE, ILE.DESTINATION FROM jiraissue JIE JOIN issuelink ILE ON (ILE.SOURCE = JIE.ID) WHERE JIE.issuetype=10000) e1 LEFT JOIN ";
        epicSQL += "(SELECT JIT.*, ILT.SOURCE, ILT.DESTINATION FROM jiraissue JIT JOIN issuelink ILT ON (ILT.DESTINATION = JIT.ID)  WHERE JIT.issuetype=10002) t1 ON (t1.ID = e1.DESTINATION) LEFT JOIN";
        epicSQL += "(SELECT JIS.*, ILS.SOURCE, ILS.DESTINATION FROM jiraissue JIS JOIN issuelink ILS ON (ILS.DESTINATION = JIS.ID) WHERE JIS.issuetype=10003) s1 ON (s1.SOURCE = t1.DESTINATION) LEFT JOIN customfieldvalue CFV ON (CFV.ISSUE = e1.id) ";
        epicSQL += "WHERE  e1.PROJECT = " + id + " AND e1.ISSUETYPE = 10000 and CFV.STRINGVALUE not in('sw_team','ghx-label-1','ghx-label-3','10000','User Stories','10400')) t GROUP BY Epic,Epic_Des";


        return db.query(epicSQL, callback);
    }
};

module.exports = epic;