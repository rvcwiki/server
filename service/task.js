var db = require('../dbconnection');
var task = {

    getTask: function (id , callback) {
        console.log(id)
        // var taskSQL;
        // taskSQL = "SELECT PROJECT, project.pname as Name, SUMMARY as Tasks, issuetype.pname as Project_Type, issuestatus.pname as Task_Status ";
        // taskSQL += "FROM (((jiraissue INNER JOIN project ON jiraissue.PROJECT = project.ID) ";
        // taskSQL += "INNER JOIN issuetype ON jiraissue.issuetype = issuetype.ID) INNER JOIN issuestatus ON jiraissue.issuestatus = issuestatus.ID) ";
        // taskSQL += "WHERE jiraissue.PROJECT = "+ id;

        var sssSQL;
         sssSQL = "SELECT e1.PROJECT AS ID, e1.ID AS SSS, CFV.STRINGVALUE AS Epic, e1.SUMMARY AS Epic_Des, SUBSTRING_INDEX(t1.SUMMARY, ':', 1) AS 'Task' ";
         sssSQL += ", SUBSTRING_INDEX(SUBSTRING_INDEX(t1.SUMMARY, ':', 2), ':', -1) AS Task_des, s1.ID AS Subtask, s1.SUMMARY AS Subtask_des, s1.REPORTER, s1.ASSIGNEE	";
         sssSQL += ",(SELECT pname FROM issuestatus JS WHERE JS.ID=s1.ISSUESTATUS) STATUS ";
         sssSQL += "FROM (SELECT JIE.*, ILE.SOURCE, ILE.DESTINATION FROM jiraissue JIE JOIN issuelink ILE ON (ILE.SOURCE = JIE.ID) WHERE JIE.issuetype=10000) e1 LEFT JOIN ";
         sssSQL += "(SELECT JIT.*, ILT.SOURCE, ILT.DESTINATION FROM jiraissue JIT JOIN issuelink ILT ON (ILT.DESTINATION = JIT.ID)  WHERE JIT.issuetype=10002) t1 ON (t1.ID = e1.DESTINATION) LEFT JOIN ";
         sssSQL += "(SELECT JIS.*, ILS.SOURCE, ILS.DESTINATION FROM jiraissue JIS JOIN issuelink ILS ON (ILS.DESTINATION = JIS.ID) WHERE JIS.issuetype=10003) s1 ON (s1.SOURCE = t1.DESTINATION) LEFT JOIN customfieldvalue CFV ON (CFV.ISSUE = e1.id) ";
         sssSQL += "WHERE  e1.PROJECT = " + id + " AND e1.ISSUETYPE = 10000 and CFV.STRINGVALUE not in('sw_team','ghx-label-1','ghx-label-3','10000','User Stories','10400')";

        return db.query(sssSQL,callback);
    }
    

};
module.exports = task;