var express = require('express');
var router = express.Router();
var login = require('../service/login');
var auth = require('../auth_module/auth');


router.post('/', function (req, res, next) {
    let dataRequest = req.body

    login.getUser(dataRequest, function (err, rows) {
        console.log(err)
        if (err !== null) {
            res.json(err);
        } else if (rows.length <= 0) {
            res.status(401).json("Can't Login");
        } else {
            let dataRes = rows[0];
            let token = auth.createToken(dataRes.ID)
            dataRes['token'] = token
            console.log(dataRes)
            res.json(dataRes);
        }
    });

});


module.exports = router;