var express = require('express');
var router = express.Router();
var search = require('../service/search');

router.post('/', function (req, res, next) {
    let dataRequest = req.body

    search.searchName(dataRequest, function (err, rows) {
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
        }
    });
});
module.exports = router;