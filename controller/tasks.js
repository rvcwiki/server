var express = require('express');
var router = express.Router();
var task = require('../service/task');

router.get('/:id', function (req, res, next) {
    let id = req.params.id
    task.getTask(id, function (err, rows) {
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            // res.send("Hello world");
            //  console.log(rows, typeof rows)
        }
    });
});


module.exports = router;