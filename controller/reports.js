var express = require('express');
var router = express.Router();
var report = require('../service/report');
var excelEX = require('../business/excelEX');
var path = require('path');

router.post('/', function (req, res, next) {
    let dataRequest = req.body
    console.log(dataRequest)

    report.getReport(dataRequest.id, function (err, rows) {
        let respones = rows;
        if (err) {
            res.json(err);
        } else {
            // res.json(rows);
            excelEX.getExcel(dataRequest.name, respones, function (err, rows) {
                if (err) {
                    res.json(err);
                } else {
                    // res.sendFile(path.resolve(__dirname,"../tmp/"+dataRequest.name+".xlsx"));
                    // let output = new File(path.resolve(__dirname, "../tmp/" + dataRequest.name + ".xlsx"));

                    res.setHeader("content-type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    res.setHeader("Content-Disposition", "attachment; filename=" + dataRequest.name + ".xlsx");
                    res.end();
                }
            });
        }
    });





    // report.getExcel(function (err, rows) {
    //     if (err) {
    //         res.json(err);
    //     } else {
    //         res.json(rows);
    //     }
    // });
});
module.exports = router;