var express = require('express')
var router = express.Router();
var epic = require('../service/epic');

router.get('/:id', function (req, res, next) {
    let id = req.params.id
    epic.getEpic(id, function (err, rows) {
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
        }
    });
});
module.exports = router;