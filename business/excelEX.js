var excelExport = {
    getExcel: function (name, data, callback) {
        var fs = require('fs');

        var officegen = require('officegen');

        var xlsx = officegen('xlsx');

        xlsx.on('finalize', function (written) {
            console.log('Finish to create an Excel file.\nTotal bytes created: ' + written + '\n');
        });

        xlsx.on('error', function (err) {
            console.log(err);
        });

        sheet = xlsx.makeNewSheet();
        sheet.name = 'Sheet 1';


        let head = Object.keys(data[0]) // Get Key Object index 0 to "head"
        sheet.data[0] = [] // Select Row index = 0
        for (let i = 0; i < head.length; i++) { //Count length "head" to "i"
            sheet.data[0][i] = head[i] //Select Row index = 0 , Column = "i" , Value in "head" index "i"
        }

        for (let row = 1; row <= data.length; row++) { //Count length "data" to "row"
            sheet.data[row] = [] // Select Row index = "row"
            let array = Object.values(data[row - 1]) //Get Value Object index "row-1" to "array" (bucause Object.values start at row[0])
            for (let col = 0; col < array.length; col++) { // Count length "array" to col
                sheet.data[row][col] = array[col] //Select Row = "row" , Column = "col" , Value in "array" index "col"
            }
        }
        // The direct option - two-dimensional array:
        // sheet.data[0] = [];
        // sheet.data[0][0] = 1;
        // sheet.data[1] = [];
        // sheet.data[1][3] = 'abc';
        // sheet.data[1][4] = 'More';
        // sheet.data[1][5] = 'Text';
        // sheet.data[1][6] = 'Here';
        // sheet.data[2] = [];
        // sheet.data[2][5] = 'abc';
        // sheet.data[2][6] = 900;
        // sheet.data[6] = [];
        // sheet.data[6][2] = 1972;

        var out = fs.createWriteStream('tmp/' + name + '.xlsx');

        out.on('error', function (err) {
            console.log(err);
        });

        xlsx.generate(out);


        callback(false, data)

    }

}
module.exports = excelExport;